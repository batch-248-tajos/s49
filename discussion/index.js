//alert("We are going to simulate an interactive web page using DOM and fetching data from a server!");

console.log("fetch() method in JS is used to send requests in the server and load the received responses in the webpage. The request and response is in JSON format");

/*
	Syntax:
		fetch("url",{option})
		url - this is the address which the req is to be made and source where the response will come from (endpoint)
		options - array of properties that contain the HTTP method, the request body and headers
*/

//Get post data from the url
fetch('https://jsonplaceholder.typicode.com/posts')
.then((response)=>response.json())
.then((data)=>showPosts(data));

//Add post data

document.querySelector('#form-add-post').addEventListener('submit',(e)=>{

	//cencel the event's default action
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts',{
		method:'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers:{'Content-Type':'application/json; charset=UTF-8'}
	})
	.then((response)=>response.json())
	.then((data)=>{

		console.log(data);
		alert('Successfully added.');

		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	})
})

//Show posts - used to display each post from JSON placeholder

const showPosts = (posts) => {

	//variable that will contain all the posts
	let postEntries = '';

	posts.forEach((post)=>{

		postEntries += `

			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;

}

// Mini-Activity:
//         Retrieve a single post from JSON API and print it in the console.


fetch("https://jsonplaceholder.typicode.com/posts/1")
.then((res)=>res.json())
.then((post)=>console.log(post))


//edit post

const editPost = (id) =>{

	//displayed post in the post section (source of data)
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	//edit post form elements (receiver of data)

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	//to remove the "disabled" attribute from the update button

	document.querySelector('#btn-submit-update').removeAttribute('disabled');
}

//Update Post

document.querySelector('#form-edit-post').addEventListener('submit',(e)=>{

	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1',{
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers:{'Content-Type':'application/json; charset=UTF-8'}
	})
	.then((response)=>response.json())
	.then((data)=>{
		console.log(data);
		alert('Successfully updated!')

		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;
		document.querySelector('#btn-submit-update').setAttribute('disabled',true);
	})
})